import os
import sys
sys.path.insert(0, os.path.join(os.path.dirname(__file__), "..", "lib"))

import fitbitlib as fitbit_client
import json
import logging
import logging.handlers
import splunklib.client as client
from splunklib.modularinput import Script, Scheme, Argument, Event
from requests_oauthlib import OAuth2Session
from datetime import datetime, date
from dateutil.relativedelta import relativedelta

callback_url = "https://localhost"
auth_url = "https://www.fitbit.com/oauth2/authorize"
token_url = "https://api.fitbit.com/oauth2/token"


class SplunkFitbit(Script):

    MASK = "********"
    APP = __file__.split(os.sep)[-3]

    def __init__(self):
        # Setup the logging handler
        self.logger = self.setup_logger(logging.INFO)

    # Setup a custom logger, logs to $SPLUNK_HOME/var/log/splunk/$APP.log.
    def setup_logger(self, level):
        app = self.APP.lower()
        logger = logging.getLogger(app)
        logger.propagate = False  # Prevent the log messages from being duplicated in the python.log file
        logger.setLevel(level)

        file_handler = logging.handlers.RotatingFileHandler("{}/var/log/splunk/{}.log".format(os.environ['SPLUNK_HOME'], app), maxBytes=25000000, backupCount=5)
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        file_handler.setFormatter(formatter)

        logger.addHandler(file_handler)

        return logger

    def get_scheme(self):
        # Setup the data inputs
        scheme = Scheme("Fitbit")
        scheme.description = "Imports Fitbit steps, heart rate, sleep and weight data."
        client_id = Argument("client_id")
        client_secret = Argument("client_secret")
        code = Argument("code")
        scheme.add_argument(client_id)
        scheme.add_argument(client_secret)
        scheme.add_argument(code)
        return scheme

    def validate_input(self, validation_definition):
        pass

    def encrypt_client_secret(self, username, password, session_key):
        args = {'token': session_key}
        service = client.connect(**args)

        try:
            for storage_password in service.storage_passwords:
                if storage_password.username == username and storage_password.realm == self.APP:
                    service.storage_passwords.delete(username=storage_password.username, realm=storage_password.realm)

            service.storage_passwords.create(password, username, self.APP)

        except Exception as e:
            raise Exception("An error occurred updating credentials. Please ensure your user account has admin_all_objects and/or list_storage_passwords capabilities. Details: {}".format(e))

    def mask_client_secret(self, session_key, client_id):
        try:
            args = {'token': session_key}
            service = client.connect(**args)
            kind, input_name = self.input_name.split("://")
            item = service.inputs.__getitem__((input_name, kind))

            kwargs = {
                "client_id": client_id,
                "client_secret": self.MASK,
            }
            item.update(**kwargs).refresh()

        except Exception as e:
            raise Exception("Error masking password: {}".format(e))

    def get_client_secret(self, session_key, username):
        args = {'token': session_key}
        service = client.connect(**args)

        for storage_password in service.storage_passwords:
            if storage_password.username == username and storage_password.realm == self.APP:
                return storage_password.content.clear_password

    def get_token(self, session_key):
        try:
            token = None
            last_updated = None
            args = {'token': session_key, 'owner': 'nobody', 'app': self.APP}
            service = client.connect(**args)

            kind, input_name = self.input_name.split("://")
            input_name = "{}".format(input_name)
            kvcollection = self.APP

            collection = service.kvstore[kvcollection]
            get_item = collection.data.query_by_id(input_name)
            if get_item:
                token = get_item['token']
                last_updated = datetime.strptime(get_item['last_updated'], "%Y-%m-%d").date()
        except Exception as e:
            self.logger.error(e)
            pass
        return token, last_updated

    def stream_events(self, inputs, ew):

        def set_token(token, new_date=None):
            '''
            Fitbit class only passes a token to the update function, creating a subfunction to
            also use session_key to save token and last_updated to Splunk REST API
            '''
            args = {'token': session_key, 'owner': 'nobody', 'app': self.APP}
            kind, input_name = self.input_name.split("://")
            input_name = "{}".format(input_name)
            kvcollection = self.APP
            last_updated = None

            service = client.connect(**args)
            collection = service.kvstore[kvcollection]

            data = collection.data.query()
            if 'last_updated' in data:
                last_updated = data[0]['last_updated']

            if new_date and new_date != last_updated:
                last_updated = new_date

            # Update record, if it fails it doesn't exist so create it.
            try:
                collection.data.update(input_name, json.dumps({'token': token, 'last_updated': last_updated}))
            except Exception:
                collection.data.insert(json.dumps({"_key": input_name, "token": token}))

        # Get inputs
        for self.input_name, self.input_item in inputs.inputs.items():
            client_id = self.input_item["client_id"]
            client_secret = self.input_item["client_secret"]
            code = self.input_item["code"]

        session_key = self._input_definition.metadata["session_key"]

        try:
            # If the password is not masked, mask it.
            if client_secret != self.MASK:
                self.encrypt_client_secret(client_id, client_secret, session_key)
                self.mask_client_secret(session_key, client_id)

        except Exception as e:
            self.logger.error("Error: {}".format(e))

        # Use clear_password for Fitbit authentication
        client_secret = self.get_client_secret(session_key, client_id)

        token, last_updated = self.get_token(session_key)

        if token is None:
            try:
                self.logger.info("No Fitbit token detected, getting one now.")
                oauth_session = OAuth2Session(client_id, redirect_uri=callback_url)
                token = oauth_session.fetch_token(token_url, code=code, client_secret=client_secret)
                self.logger.debug("Got token: {}".format(json.dumps(token)))
                set_token(token)
            except Exception as e:
                self.logger.error("Possibly invalid API key, get a new one from https://www.fitbit.com/oauth2/authorize?response_type=code&client_id={}&redirect_uri=https://localhost&scope=profile%20weight%20heartrate%20sleep%20activity".format(client_id))
                self.logger.error(e)
                sys.exit(1)

        access_token = token['access_token']
        refresh_token = token['refresh_token']
        expires_at = token['expires_at']

        # Initialise Fitbit client and set refresh options. Use API v1.2, required for e.g. sleep data.
        fitbit = fitbit_client.Fitbit(client_id, client_secret, access_token, refresh_token, expires_at=expires_at, refresh_cb=set_token)
        fitbit.API_VERSION = 1.2

        # Get user profile for time offset, set locale for API calls. Exit if it fails, rest of script relies on this.
        try:
            response_profile = fitbit.user_profile_get()
            fitbit.system = response_profile['user']['locale']
            member_since = response_profile['user']['memberSince']
            utc_offset = response_profile['user']['offsetFromUTCMillis']
        except Exception as e:
            self.logger.error("Couldn't get Fitbit profile: {}".format(e))
            self.logger.error(response_profile)
            sys.exit(1)

        # Set start_date to date from KV Store or to Fitbit registration date if it doesn't exist
        start_date = last_updated or datetime.strptime(member_since, "%Y-%m-%d").date()

        # Set end date to yesterday, as we don't want to get steps or heart rate for a day still in progress.
        end_date = date.today() - relativedelta(days=1)
        sleep_end_date = end_date

        # If start date & end date are the same, no need to do anything until the next day.
        if start_date == end_date:
            self.logger.info("All Fitbit stats are up to date until {}. New stats tomorrow.".format(start_date))
            sys.exit(0)

        # Set period to '1d' when we have done the initial pull, set sleep_start_time accordingly. Otherwise set it 1 month for most, 100 days for sleep.
        if start_date == last_updated:
            period = '1d'
            sleep_start_date = last_updated
            # If last_updated is more than 100 days ago, set sleep_end_date to 100 days from start_date which is max. Eventually this will get all sleep.
            if (end_date - sleep_start_date).days > 100:
                sleep_end_date = sleep_start_date + relativedelta(days=100)
        # else gets hit if we haven't done the initial pull. Can only get max '1m', or 100 days for sleep data.
        else:
            period = '1m'
            sleep_start_date = date.today() - relativedelta(days=100)

        try:
            self.logger.info("Getting Fitbit data now...")
            # Sleep API call is different, using a start & end date and max 100 days. So taking today - 100 days as start.
            response_sleep = fitbit.get_sleep_range(sleep_start_date, sleep_end_date)
            for item in response_sleep['sleep']:
                event = Event(stanza=self.input_name, sourcetype='fitbit:sleep', data=json.dumps(item))
                ew.write_event(event)

            has_more = True
            while has_more:

                self.logger.info("Getting data for {} ({} period)".format(start_date, period))

                if start_date == end_date:
                    has_more = False

                response_weight = fitbit.get_bodyweight(base_date=start_date, period=period)
                response_hr = fitbit.time_series('activities/heart', base_date=start_date, period=period)
                response_steps = fitbit.time_series('activities/steps', base_date=start_date, period=period)
                # time will be off during daylight savings and historic fetch - drawback of Fitbit time being local and not UTC
                for item in response_weight['weight']:
                    # Add a logIdUTC field which has timestamp in UTC to assist Splunk indexing.
                    item['logIdUTC'] = int(item['logId']) - utc_offset
                    event = Event(stanza=self.input_name, sourcetype='fitbit:body', data=json.dumps(item))
                    ew.write_event(event)

                for item in response_hr['activities-heart']:
                    # If restingHeartRate not in item, filter it as it's empty data.
                    if 'restingHeartRate' in item:
                        event = Event(stanza=self.input_name, sourcetype='fitbit:heartrate', data=json.dumps(item))
                        ew.write_event(event)

                for item in response_steps['activities-steps']:
                    # If value is 0, tracker likely not in use (yet) and not to be included.
                    if item['value'] != "0":
                        event = Event(stanza=self.input_name, sourcetype='fitbit:steps', data=json.dumps(item))
                        ew.write_event(event)

                # Get token, last_updated in case the token changed in the mean time. Then update last_updated in KV Store with start_time
                token, last_updated = self.get_token(session_key)
                new_date = start_date.strftime("%Y-%m-%d")
                set_token(token, new_date=new_date)

                if period == '1d':
                    start_date += relativedelta(days=+1)
                else:
                    start_date += relativedelta(months=+1)

                if start_date > end_date and has_more:
                    start_date = end_date

            self.logger.info("Looks like we have all Fitbit data.")
        except Exception as e:
            self.logger.error(e)


if __name__ == "__main__":
    sys.exit(SplunkFitbit().run(sys.argv))
