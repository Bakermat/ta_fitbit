##### Summary
Use this add-on to get Fitbit steps, heart rate, sleep and weight data into Splunk.

It uses distinct source types for each type:

- fitbit:body for weight & fat data from Aria scales.
- fitbit:heartrate for heart rate data.
- fitbit:sleep for sleep data.
- fitbit:steps for, you guessed it, steps data.

##### Requirements
- This add-on uses Python 3 and will only work on Splunk Enterprise 8.x or newer.
- Create your own Fitbit app if you don't have one already, which can be done [here](https://dev.fitbit.com/apps/new/).
- **Important**: make sure to set the Callback URL to https://localhost in your Fitbit app settings.

##### Usage
- Install the Fitbit TA for Splunk.
- Create a new Fitbit input, by going to Settings -> Data Inputs -> Fitbit. By default it runs once every day (86400 seconds).
- Copy the Client ID and Client Secret from your Fitbit app above to the respective fields.
- The API code can be retrieved by going to [this URL](https://www.fitbit.com/oauth2/authorize?response_type=code&client_id=******&redirect_uri=https://localhost&scope=weight profile heartrate activity sleep), replace the asterisks with your Client ID. The API code is only valid for 10 minutes.
- For the TA to work properly, allow access to all proposed items (profile, heart rate, sleep, steps and weight).
- Once you submit, you'll get redirected to a non-existent page on https://localhost which is expected. Copy the string between 'code=' and '#', which is the API code. So for example, if the URL is https://localhost/?code=xxx#_=_ then the code is 'xxx'.
- To get accurate data, make sure your Fitbit profile has the correct settings for timezone and language. These settings determine if you get stats based on US, UK or metric units as per [Fitbit's API documentation](https://dev.fitbit.com/build/reference/web-api/basics/).

##### Normalisation
The following sourcetypes capture their main metric as follows to normalise data across other apps from the same author, e.g. Garmin Add-On for Splunk & Fitness App for Splunk:

- fitbit:sleep - sleepDuration
- fitbit:steps - steps

##### Known caveats
- During the first time pull of historic data, the time logged for weight might be slightly off. This is due to Fitbit storing time without any timezone information, so the TA uses the time from your profile to make an educated guess. This is based on today's time and will be slighly off for historic times if the offset at the time was different due to daylight savings.
- I don't own a Fitbit fitness tracker, so wasn't able to test the sleep, steps or heart rate data first hand. You can use Splunk Community if you see anything unusual, please include as much detail as possible.

##### Troubleshooting
- If you want to start over again, make sure to regenerate the API key as it can only be used once. You will have to delete the KV Store entry which is used to store the tokens. You can do this by using the Lookup Editor and deleting the relevant input or by running 'splunk clean  kvstore -app TA_garmin -collection TA_garmin' from CLI, which will delete ALL inputs in case you have multiple accounts configured.
- The API key can only be used once, so make sure to generate a new API key if you want to reindex data or start over again.

##### Acknowledgements
- Icon made by [Freepik](https://www.freepik.com) from [Flaticon](https://www.flaticon.com/).
- This TA uses https://github.com/orcasgit/python-fitbit for interacting with the Fitbit API, with some modifications.
- This TA uses https://github.com/requests/requests-oauthlib as OAuth client.